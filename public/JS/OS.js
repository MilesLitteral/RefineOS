var set = {name:"name", key:"key", tokens:"tokens", msg:"msg", w1:"w1", w2:"w2"};

function post(info, path){//template
	d3.request(path) //ex: "/newUser", "/test", "/" 
    .header("Content-Type", "application/json")
    .post(JSON.stringify(info), function(x){console.log(x.response);});
}

function readData(data){
	var pos = 0;//used in the for loop
	for(i in set){
		set[i] = data[pos].value; //reads data value from the DOM element
		pos++;
	}
	//console.log(set);
	post(set, "/newUser");
}

function retrieve(){
	d3.request("/test")
		.get(function(x){
			//console.log(x);
			console.log("the server returned: " + x.response);
		});
}

console.log("running from 'public' dir");
