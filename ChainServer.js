var express = require('express');
var app = express();
var bodyParser = require('body-parser');

//sets the path that express will read files from
app.use(express.static(__dirname + '/public'));

//setup parser for use in express
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());



app.get('/', function(req, res){//default path
  res.sendFile(__dirname + '/index.html');
  //res.sendStatus(res.statusCode);
});


app.get('/test', function(req, res){
	console.log("/test hit");
	console.log(req.body);
	res.send({x:3, y: 4, w:"g"});
});


app.post('/newUser', function(req, res){//the incoming data firsts need to be examined for security treats before it's stored in the server

	console.log("/newUser hit");
	console.log(req.body);
	//console.log('received: ' + JSON.parse(req.body) );
	res.sendStatus(res.statusCode);
});


app.listen(3000, function(){
		console.log('Listening on port 3000!');
	});	
